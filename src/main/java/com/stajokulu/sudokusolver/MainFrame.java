/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stajokulu.sudokusolver;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingWorker;
import javax.swing.border.EmptyBorder;

import java.awt.GridLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.BoxLayout;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.Component;
import java.awt.CardLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Color;
import java.awt.HeadlessException;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.border.LineBorder;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.highgui.Highgui;
import org.opencv.highgui.VideoCapture;
/**
 *
 * @author muhammet
 */
public class MainFrame extends javax.swing.JFrame {


    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        camArea = new javax.swing.JPanel();
        sudokuArea = new javax.swing.JPanel();
        openCamBtn = new javax.swing.JButton();
        grabSudokuBtn = new javax.swing.JButton();
        solveSudokuBtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("StajOkulu17 Sudoku Solver");
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setIconImages(null);
        setMaximumSize(new java.awt.Dimension(840, 490));
        setMinimumSize(new java.awt.Dimension(840, 490));
        setPreferredSize(new java.awt.Dimension(840, 490));
        setResizable(false);
        setSize(new java.awt.Dimension(840, 490));

        camArea.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        camArea.setMaximumSize(new java.awt.Dimension(400, 400));
        camArea.setMinimumSize(new java.awt.Dimension(400, 400));
        camArea.setPreferredSize(new java.awt.Dimension(400, 400));

        javax.swing.GroupLayout camAreaLayout = new javax.swing.GroupLayout(camArea);
        camArea.setLayout(camAreaLayout);
        camAreaLayout.setHorizontalGroup(
            camAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );
        camAreaLayout.setVerticalGroup(
            camAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );

        sudokuArea.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        sudokuArea.setMaximumSize(new java.awt.Dimension(400, 400));
        sudokuArea.setMinimumSize(new java.awt.Dimension(400, 400));

        javax.swing.GroupLayout sudokuAreaLayout = new javax.swing.GroupLayout(sudokuArea);
        sudokuArea.setLayout(sudokuAreaLayout);
        sudokuAreaLayout.setHorizontalGroup(
            sudokuAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );
        sudokuAreaLayout.setVerticalGroup(
            sudokuAreaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 398, Short.MAX_VALUE)
        );

        openCamBtn.setText("Open Cam");
        openCamBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openCamBtnActionPerformed(evt);
            }
        });

        grabSudokuBtn.setText("Grab Sudoku");
        grabSudokuBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                grabSudokuBtnActionPerformed(evt);
            }
        });

        solveSudokuBtn.setText("Solve!");
        solveSudokuBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                solveSudokuBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(openCamBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(grabSudokuBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(camArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sudokuArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(solveSudokuBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(sudokuArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(camArea, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(openCamBtn)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(solveSudokuBtn)
                        .addComponent(grabSudokuBtn)))
                .addContainerGap(38, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void openCamBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openCamBtnActionPerformed
        webSource = new VideoCapture(0);
				//hangi kameranin kullanilacagi
				//0 = birincil kamera
				//mobil cihaz bağlayarak 1 yapilir
				myThread = new MainFrame.DaemonThread();
				Thread t = new Thread(myThread);
				t.setDaemon(true);
				myThread.runnable = true;
				t.start();
				openCamBtn.setEnabled(false); // start button
				
				grabSudokuBtn.setEnabled(true); // stop button
        
        
    }//GEN-LAST:event_openCamBtnActionPerformed

    private void grabSudokuBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_grabSudokuBtnActionPerformed
        myThread.runnable = false;
				openCamBtn.setEnabled(true);
				grabSudokuBtn.setEnabled(false);
				solveSudokuBtn.setEnabled(true);
				webSource.release();
				
				try{
					Highgui.imencode(".bmp", frame, mem);
					Image im = ImageIO.read(new ByteArrayInputStream(
							mem.toArray()));
	
					BufferedImage buff = (BufferedImage) im;
					Graphics g = sudokuArea.getGraphics();
	
					
					if (g.drawImage(buff, 0, 0, getWidth()-600,
							getHeight()-200, 0, 0, buff.getWidth(),
							buff.getHeight(), null));
					image.setImg(buff);
				}catch(IOException e){
					e.printStackTrace();
				}
        
        
        
    }//GEN-LAST:event_grabSudokuBtnActionPerformed

    private void solveSudokuBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_solveSudokuBtnActionPerformed
        BufferedImage img = image.getImg();
				int rows=img.getHeight();
				int cols=img.getWidth();
				int type=CvType.CV_8UC3;
				byte[] pixels = ((DataBufferByte)img.getRaster().getDataBuffer()).getData();
				Mat src = new Mat(rows,cols,type);
				src.put(0, 0, pixels);
				
				src = grid.detectGrid(src);
				
				
				//src = digits.detectDigits(src);
				//digits.detect(src);
    }//GEN-LAST:event_solveSudokuBtnActionPerformed

     static {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	};
	//kutuphaneleri eklemek gerekiyor
	
	
	
	DetectDigits digits = new DetectDigits();
	DetectSudokuGrid grid = new DetectSudokuGrid();
	GrabbedImage image = new GrabbedImage();
	
	private MainFrame.DaemonThread myThread = null;
	int count = 0;
	VideoCapture webSource = null;

	Mat frame = new Mat();
	MatOfByte mem = new MatOfByte();
        
        
    
    public static void main(String args[]) {
     EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
    }

    public MainFrame() throws InterruptedException {
        
		

        initComponents();
				
    }

    class DaemonThread implements Runnable {
		protected volatile boolean runnable = false;

		public void run() {
			synchronized (this) {
				while (runnable) {
					if (webSource.grab()) {
						try {
							webSource.retrieve(frame);
							
							Highgui.imencode(".bmp", frame, mem);
							Image im = ImageIO.read(new ByteArrayInputStream(
									mem.toArray()));

							BufferedImage buff = (BufferedImage) im;
							Graphics g = camArea.getGraphics();

							
							if (g.drawImage(buff, 0, 0, getWidth()-600,
									getHeight()-200 , 0, 0, buff.getWidth(),
									buff.getHeight(), null))

								if (runnable == false) {
									System.out.println("Going to wait()");
									this.wait();
								}
						} catch (Exception ex) {
							System.out.println("Error");
						}
					}
				}
			}
		}
	}
    
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel camArea;
    private javax.swing.JButton grabSudokuBtn;
    private javax.swing.JButton openCamBtn;
    private javax.swing.JButton solveSudokuBtn;
    private javax.swing.JPanel sudokuArea;
    // End of variables declaration//GEN-END:variables
}
