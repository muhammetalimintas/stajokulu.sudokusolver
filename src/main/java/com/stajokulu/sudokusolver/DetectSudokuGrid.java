/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.stajokulu.sudokusolver;

/**
 *
 * @author muhammet
 */
import java.util.ArrayList;
import java.util.List;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;

import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;


public class DetectSudokuGrid {
	public static Mat detectGrid(Mat src){
		
		Mat gray = new Mat();
		
		Imgproc.cvtColor(src, gray, Imgproc.COLOR_BGR2GRAY);
		Highgui.imwrite("output/gray.png", gray);
		
		Imgproc.GaussianBlur(gray, gray, new Size(11, 11), 0);
        Highgui.imwrite("output/gauss.png", gray);

		Imgproc.adaptiveThreshold(gray, gray, 255, Imgproc.ADAPTIVE_THRESH_MEAN_C, Imgproc.THRESH_BINARY_INV, 5, 2);
        Highgui.imwrite("output/thresholded.png", gray);

		List<MatOfPoint> contours = new ArrayList<>();
        Mat heirarchy = new Mat();
        Imgproc.findContours(gray, contours, heirarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE, new Point(0, 0));
        Highgui.imwrite("output/contours.png", gray);

        int h = src.height();
        int w = src.width();
        
        Double max = (double) 0;
        MatOfPoint2f biggest = new MatOfPoint2f();
        
        for (MatOfPoint m : contours) {
            MatOfPoint2f a = new MatOfPoint2f();
            
            Imgproc.approxPolyDP(new MatOfPoint2f(m.toArray()), a, 4, true);
            if (a.total() != 4) {
                continue;
            }
            MatOfPoint am = new MatOfPoint();
            a.convertTo(am, CvType.CV_32S);
            if (!Imgproc.isContourConvex(am)) {
                continue;
            }
            Double sizeApprox = Imgproc.contourArea(a);
            if (sizeApprox > max) {
                max = sizeApprox;
                biggest = a;
            }
        }
        Point[] points = biggest.toArray();
        Mat copyImage = src.clone();
        Core.line(copyImage, new Point(points[0].x, points[0].y), new Point(points[1].x, points[1].y), new Scalar(255, 0, 0), 2);
        Core.line(copyImage, new Point(points[1].x, points[1].y), new Point(points[2].x, points[2].y), new Scalar(255, 0, 0), 2);
        Core.line(copyImage, new Point(points[2].x, points[2].y), new Point(points[3].x, points[3].y), new Scalar(255, 0, 0), 2);
        Core.line(copyImage, new Point(points[3].x, points[3].y), new Point(points[0].x, points[0].y), new Scalar(255, 0, 0), 2);
        Highgui.imwrite("output/line.png", copyImage);

        List<Point> pointList = biggest.toList();
        int y = 0, s2 = 0;
        double min = points[0].y;
        for (int i = 1; i < 4; i++) {
            if (min > points[i].y) {
                y = i;
                min = points[i].y;
            }
        }
        boolean flag = false;
        for (int i = 0; i < 4; i++) {
            if (i != y) {
                if (!flag) {
                    min = Math.abs(points[y].y - points[i].y);
                    flag = true;
                    s2 = i;
                } else if (min > Math.abs(points[y].y - points[i].y)) {
                    s2 = i;
                    min = Math.abs(points[y].y - points[i].y);
                }
            }
        }
        List<Point> pointListOri = new ArrayList<>();
        if (points[y].x > points[s2].x) {
            pointListOri.add(points[s2]);
            pointListOri.add(points[y]);
        } else {
            pointListOri.add(points[y]);
            pointListOri.add(points[s2]);
        }
        Integer[] a = {0, 0, 0, 0};
        a[y] = 1;
        a[s2] = 1;
        int j = 0;
        Integer[] s3 = new Integer[2];
        for (int i = 0; i < 4; i++) {
            if (a[i] == 0)
                s3[j++] = i;
        }
        if (points[s3[0]].x > points[s3[1]].x) {
            pointListOri.add(points[s3[0]]);
            pointListOri.add(points[s3[1]]);
        } else {
            pointListOri.add(points[s3[1]]);
            pointListOri.add(points[s3[0]]);
        }
        
        MatOfPoint2f s = new MatOfPoint2f();
        s.fromList(pointListOri);
        Size reshape = new Size(180, 180);
        Mat undistorted = new Mat(reshape, CvType.CV_8UC1);
        //Highgui.imwrite("output/undistorted.png", undistorted);

        MatOfPoint2f d = new MatOfPoint2f();
        d.fromArray(new Point(0, 0), new Point(reshape.height, 0),
                new Point(reshape.width, reshape.height), new Point(0, reshape.width));
        Mat tranformMatrix = Imgproc.getPerspectiveTransform(s, d);
        //Highgui.imwrite("output/transfrommatrix.png", tranformMatrix);
        
        Imgproc.warpPerspective(src, undistorted, tranformMatrix, reshape);
        Highgui.imwrite("output/undistorted2.png", undistorted);
		
		return undistorted;
		
	}
}
